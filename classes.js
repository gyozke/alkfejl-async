"use strict";

class Pont {

  constructor (x, y) {
    this.x = x;
    this.y = y;
  }

  setX(value) {
    this.x = value;
  }

  getX() {
    return this.x;
  }

  set X(value) {
    this.x = value;
  }

  get X() {
    return this.x;
  }
}

class Kor extends Pont {
  constructor (x, y, r) {
    super(x, y);
    this.r = r;
  }

  toString() {
    console.log(this.x, this.y, this.r);
  }
}

const p = new Pont(10, 10);
p.X = 20;
console.log(p.x, p.X);

const k = new Kor(20, 20, 10);
k.toString();

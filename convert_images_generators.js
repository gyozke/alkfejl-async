const fs = require('fs');
const jimp = require('jimp');
const Datastore = require('nedb-promise');
const co = require('co');

const db = new Datastore({ filename: 'images.nedb', autoload: true });
const path = 'images/';
const convertedPath = 'converted';

function readdir(path) {
  return new Promise(function (resolve, reject) {
    fs.readdir(path, function (err, files) {
      if (err) return reject(err);
      resolve(files);
    });
  });
}

co(function* () {
  try {
    const numRemoved = yield db.remove({}, { multi: true });
    console.log(numRemoved, 'was removed');
    const files = yield readdir(path);
    for (let fileName of files) {
      const image = yield jimp.read(path + fileName);
      const { width, height } = image.bitmap;
      const insertedImage = yield db.insert({ fileName, width, height });
      image.resize(100, jimp.AUTO);
      image.write(`${convertedPath}/${insertedImage._id}.png`);
      console.log(fileName, 'saved and converted')
    };
    console.log('all ended????');
  }
  catch (err) {
    console.log(err)
  }
});

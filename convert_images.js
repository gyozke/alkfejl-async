const fs = require('fs');
const jimp = require('jimp');
const Datastore = require('nedb');

const db = new Datastore({ filename: 'images.nedb', autoload: true });
const path = 'images/';
const convertedPath = 'converted';

db.remove({}, { multi: true }, function (err, numRemoved) {
  console.log(numRemoved, 'was removed');
  fs.readdir(path, function (err, files) {
    // console.log(files);
    files.forEach(function (fileName) {
      jimp.read(path + fileName, function (err, image) {
        // console.log(fileName, image.bitmap.width, image.bitmap.height);
        const { width, height } = image.bitmap;
        db.insert({ fileName, width, height }, function (err, insertedImage) {
          // console.log(insertedImage)
          image.resize(100, jimp.AUTO);
          image.write(`${convertedPath}/${insertedImage._id}.png`, function (err) {
            if (err) throw err;
            console.log(fileName, 'saved and converted')
          });
        });
      });
    });
    console.log('all ended????');
  });
});



/*
db törlése
könyvtárban lévő fájlok
mindegyik fájlra
    beolvas
    beszúr
    átméretez
    kiír
kiír
*/
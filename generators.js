function* genFn() {
  const a = yield 1;
  const b = yield 2;
  yield a + b;
  return 3;
}

const it = genFn();
console.log(it.next())
console.log(it.next(4))
console.log(it.next(10))
console.log(it.next())
console.log(it.next())
console.log(it.next())
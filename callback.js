function hello(name) {
  console.log('hello', name);
}

function longOperation(ms, cb) {
  setTimeout(cb, ms)
}


longOperation(1000, () => hello(1));
hello(2);

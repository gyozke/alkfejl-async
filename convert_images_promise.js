const fs = require('fs');
const jimp = require('jimp');
const Datastore = require('nedb-promise');

const db = new Datastore({ filename: 'images.nedb', autoload: true });
const path = 'images/';
const convertedPath = 'converted';

function readdir(path) {
  return new Promise(function (resolve, reject) {
    fs.readdir(path, function (err, files) {
      if (err) return reject(err);
      resolve(files);
    });
  });
}

db.remove({}, { multi: true }).then(function (numRemoved) {
  console.log(numRemoved, 'was removed');
  readdir(path)
    .then(function (files) {
      // console.log(files);
      return Promise.all(files.map(function (fileName) {
        return jimp.read(path + fileName).then(function (image) {
          const { width, height } = image.bitmap;
          return db.insert({ fileName, width, height }).then(function (insertedImage) {
            // console.log(insertedImage)
            image.resize(100, jimp.AUTO);
            return image.write(`${convertedPath}/${insertedImage._id}.png`, function () {
              console.log(fileName, 'saved and converted')
            });
          });
        });
      }));
    })
    .then(function (values) {
      console.log('all ended');
    })
    .catch(function (err) {
      console.log(err)
    })
});

/*
    jimp.read(path + fileName, function (err, image) {
        // console.log(fileName, image.bitmap.width, image.bitmap.height);
        const { width, height } = image.bitmap;
        db.insert({ fileName, width, height }, function (err, insertedImage) {
            // console.log(insertedImage)
            image.resize(100, jimp.AUTO);
            image.write(`${convertedPath}/${insertedImage._id}.png`, function (err) {
                if (err) throw err;
                console.log(fileName, 'saved and converted')
            });
        });
    });

*/
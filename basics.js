"use strict";

function faktorialis(n) {
  let f = 1;
  for (let i = 2; i <= n; i++) {
    f *= i;
  }
  return f;
}

function eldontes(x) {
  let i = 0;
  while (i < x.length && !(x[i] < 0)) {
    i++;
  }
  return i < x.length;
}

console.log(faktorialis(5));
console.log(eldontes([1,2,3,34,342,3,4]))
console.log([1,2,3,34,342,3,4].some(i => i < 0))
console.log([1,2,3,34,342,-3,4].some(i => i < 0))




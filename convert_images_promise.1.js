const fs = require('fs');
const jimp = require('jimp');
const Datastore = require('nedb-promise');

const db = new Datastore({ filename: 'images.nedb', autoload: true });
const path = 'images/';
const convertedPath = 'converted';

function readdir(path) {
  return new Promise(function (resolve, reject) {
    fs.readdir(path, function (err, files) {
      if (err) return reject(err);
      resolve(files);
    });
  });
}

function processFile(fileName) {
  let theImage;
  return jimp.read(path + fileName)
    .then(function (image) {
      theImage = image;
      const { width, height } = image.bitmap;
      return db.insert({ fileName, width, height })
    })
    .then(function (insertedImage) {
      theImage.resize(100, jimp.AUTO);
      return theImage.write(`${convertedPath}/${insertedImage._id}.png`);
    })
    .then(function () {
      console.log(fileName, 'saved and converted')
      return;
    });
}

db.remove({}, { multi: true })
  .then(function (numRemoved) {
    console.log(numRemoved, 'was removed');
    return;
  })
  .then(function () {
    return readdir(path);
  })
  .then(function (files) {
    return Promise.all(files.map(processFile));
  })
  .then(function (values) {
    console.log(values);
    console.log('all ended');
    return;
  })
  .catch(function (err) {
    console.log(err);
  });


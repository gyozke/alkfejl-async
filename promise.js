function hello(name) {
  console.log('hello', name);
}


setTimeout(() => hello(1), 1000);


function waitFor(ms) {
  return new Promise(function (resolve, reject) {
    setTimeout(resolve, ms)
  });
}

waitFor(1000).then(() => hello(2));